package go.ana.behavioral.patterns.cake.strategy;

import java.math.BigDecimal;

public class StrawberryCake implements Cake {

	@Override
	public BigDecimal getCakeAmount() {
		return new BigDecimal(30);
	}

}
