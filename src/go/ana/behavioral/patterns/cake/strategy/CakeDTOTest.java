package go.ana.behavioral.patterns.cake.strategy;

import java.math.BigDecimal;

public class CakeDTOTest {


	public static void main(String[] args) {
		testCakeAmount();
		testCakeAmountStrategy();
	}

	private static void testCakeAmountStrategy() {
		System.out.println("Using strategy strawberry: "+ new StrawberryCake().getCakeAmount());
		System.out.println("Using strategy banana: "+ new BananaCake().getCakeAmount());
		System.out.println("Using strategy chocolate: "+ new ChocolateCake().getCakeAmount());


		
	}
	
	private static void testCakeAmount() {
		System.out.println("strawberry: "+ getCakeAmount(new CakeDTO("strawberry")));
		System.out.println("banana: "+ getCakeAmount(new CakeDTO("banana")));
		System.out.println("chocolate: "+ getCakeAmount(new CakeDTO("chocolate")));
	}
	
	
	private static BigDecimal getCakeAmount(CakeDTO cake) {
		if(cake.name.equals("strawberry")) 
			return new BigDecimal(30);

		if(cake.name.equals("banana")) 
			return new BigDecimal(25);

		if(cake.name.equals("chocolate")) 
			return new BigDecimal(40);

		return new BigDecimal(15);
	}

}
