package go.ana.behavioral.patterns.cake.strategy;

import java.math.BigDecimal;

public class BananaCake implements Cake {

	@Override
	public BigDecimal getCakeAmount() {
		return new BigDecimal(25);
	}

}
