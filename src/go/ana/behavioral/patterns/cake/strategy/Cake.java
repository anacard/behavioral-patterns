package go.ana.behavioral.patterns.cake.strategy;

import java.math.BigDecimal;

public interface Cake {
	public BigDecimal getCakeAmount(); 
}
